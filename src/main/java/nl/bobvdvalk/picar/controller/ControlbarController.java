package nl.bobvdvalk.picar.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ControlbarController implements Initializable {

    private static final Logger LOG = Logger.getLogger(ControlbarController.class);

    @FXML
    private Tab tabs;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            Tab phoneTab = new Tab();
            tabs.getTabPane().getTabs().add(phoneTab);
            phoneTab.setContent(FXMLLoader.load(getClass().getClassLoader().getResource("gui/phone.fxml")));
            phoneTab.setText("Phone");

            Tab navigationTab = new Tab();
            tabs.getTabPane().getTabs().add(navigationTab);
            navigationTab.setContent(FXMLLoader.load(getClass().getClassLoader().getResource("gui/navigation.fxml")));
            navigationTab.setText("Navigation");

            Tab settingsTab = new Tab();
            tabs.getTabPane().getTabs().add(settingsTab);
            settingsTab.setContent(FXMLLoader.load(getClass().getClassLoader().getResource("gui/menu.fxml")));
            settingsTab.setText("Settings");
        } catch (IOException e) {
            LOG.error("cannot load fxml into anchorpane");
        }
    }
}
