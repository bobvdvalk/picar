package nl.bobvdvalk.picar.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.net.URL;
import java.util.ResourceBundle;

public class NavigationController implements Initializable {

    @FXML
    private WebView webview;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
       WebEngine webEngine = webview.getEngine();
       webEngine.load("https://maps.google.com");
    }
}
