package nl.bobvdvalk.picar.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class MenuController implements Initializable {

    private static final Logger LOG = Logger.getLogger(MenuController.class);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        LOG.info("menu started");
    }

    @FXML
    public void radioBtnPressed() {
        LOG.info("button: radio");
    }

    public void navBtnPressed(ActionEvent actionEvent) {
        LOG.info("button: navigation");
    }

    public void phoneBtnPressed(ActionEvent actionEvent) {
        LOG.info("button: phone");
    }

    public void settingsBtnPressed(ActionEvent actionEvent) {
        LOG.info("button: settings");
    }

    public void spotifyBtnPressed(ActionEvent actionEvent) {
        LOG.info("spotifyBtnPressed");
    }
}
